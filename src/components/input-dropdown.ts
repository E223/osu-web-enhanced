import { createElement } from "../../typescript-lib/src/shared/dom"
import { osuTheme } from "../../typescript-lib/src/shared/themes"

/**
 * This object defines a dropdown selector input.
 */
export class OsuInputDropdown extends HTMLLabelElement {
    /**
     * The HTMLSelectElement of this component
     */
    private selector: HTMLSelectElement

    /**
     * constructor
     */
    constructor() {
        super()
        this.className = "form-select"
        this.style.backgroundColor = osuTheme.secondaryBackground

        this.selector = createElement("select", {
            className: "form-select__input",
            style: {
                minWidth: `calc(${osuTheme.inputMinWidth} - 20px)`,
                padding: "4px 24px 4px 4px",
                lineHeight: "19px",
                fontSize: "14px",
                color: osuTheme.normalTextColor,
                backgroundColor: osuTheme.secondaryBackground,
                borderRadius: "4px",
            },
        })
        this.append(this.selector)
    }

    /**
     * This function returns the HTMLSelectElement of this component
     * @returns HTMLSelectElement
     */
    public getSelector() {
        return this.selector
    }

    /**
     * This function appends a HTMLOptionElement to this components selector
     * @param option HTMLOptionElement to append
     */
    public appendOption(option: HTMLOptionElement) {
        this.selector.append(option)
    }
}
