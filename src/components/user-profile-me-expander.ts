import { ensuredSelector } from "../../typescript-lib/src/shared/common"
import { createElement, createIcon, insertStyleTag } from "../../typescript-lib/src/shared/dom"
import { osuTheme } from "../../typescript-lib/src/shared/themes"

/**
 * This component defines the me! section expander
 */
export class UserProfileMeExpander extends HTMLDivElement {
    /**
     * Label of the expander
     */
    private label: HTMLSpanElement

    /**
     * This property defines what action the MeSectionExpander is performing on its next trigger
     * @see state
     */
    private state: state

    /**
     * constructor
     */
    constructor() {
        super()

        this.classList.add("me-expander")

        this.style.display = "flex"
        this.style.flexDirection = "column"
        this.style.alignItems = "center"
        this.style.cursor = "pointer"
        this.style.borderTop = `1px solid ${osuTheme.osuPink}`
        this.style.position = "absolute"
        this.style.marginLeft = "-50px"
        this.style.userSelect = "none"
        this.style.right = "0"
        this.style.bottom = "0"
        this.style.width = "100%"
        this.style.fontWeight = "700"
        this.style.backgroundColor = "inherit"

        this.label = createElement("span", {
            attributes: {
                innerText: "Expand",
            },
        })
        this.state = "expand"

        this.onclick = () => {
            this.toggleState()
        }

        insertStyleTag(`
        .me-expander:hover {
            filter: brightness(1.2)
        }
        .me-expander.expanded svg {
            transform: rotate(180deg)
        }
    `)

        this.append(this.label, createIcon("chevronDown"))

        insertStyleTag(`
        .me-expander:hover {
            background-color: ${osuTheme.secondaryBackground}
        }
        .me-expander.expanded svg {
            transform: rotate(180deg)
        }
    `)
    }

    /**
     * This function toggles the state of the expander. This includees:
     * - this.state
     * - expand/foldin userpage
     * - turn chevron
     * - update label
     * @param state
     */
    public toggleState(state: state = this.state) {
        if (state == "expand") {
            try {
                this.expandMeSection()
            } catch {}
            this.updateLabel("Collapse")
            this.state = "collapse"
            this.classList.add("expanded")
        } else if (state == "collapse") {
            try {
                this.foldInMeSection()
            } catch {}
            this.updateLabel("Expand")
            this.state = "expand"
            this.classList.remove("expanded")
        }
    }

    /**
     * This function updates the label of the expander
     * @param label
     */
    private updateLabel(label: string) {
        this.label.innerText = label
    }

    /**
     * This function expands the userpage
     */
    private expandMeSection() {
        const mainSection = ensuredSelector<HTMLElement>(".page-extra__content-overflow-wrapper-inner")
        mainSection.style.maxHeight = "unset"
        mainSection.style.overflowY = "scroll"
        ensuredSelector<HTMLElement>(".page-extra__content-overflow-wrapper-outer").style.maxHeight = "unset"
    }

    /**
     * This function folds in the userpage
     */
    private foldInMeSection() {
        ensuredSelector<HTMLElement>(".page-extra__content-overflow-wrapper-inner").style.maxHeight = "400px"
        ensuredSelector<HTMLElement>(".page-extra__content-overflow-wrapper-outer").style.maxHeight = "400px"
    }
}

/**
 * This type defines what action the MeSectionExpander is performing on its next trigger
 */
type state = "expand" | "collapse"
