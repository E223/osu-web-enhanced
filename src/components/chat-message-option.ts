import { CustomMessage } from "../constants/types"

/**
 * This object defines an element of the message selection dropdown.
 */
export class MessageSelectOption extends HTMLOptionElement {
    /**
     * CustomMessage object.
     */
    private customMessage: CustomMessage

    /**
     * Constructor, assign a CustomMessage to this object.
     * @param customMessage The CustomMessage to be hold by this object.
     */
    constructor(customMessage: CustomMessage) {
        super()

        this.innerText = customMessage.messageOptions.title
        this.customMessage = customMessage

        customMessage.messageOptions.selected === true ? (this.selected = true) : (this.selected = false)
    }

    /**
     * @returns the full CustomMessage object for this message.
     */
    public getCustomMessage() {
        return this.customMessage
    }
}
