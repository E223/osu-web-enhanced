import { ensuredSelector } from "../typescript-lib/src/shared/common"

/**
 * handles notification display
 */
export function displayNotifications() {
    const channel = new BroadcastChannel("osu-web_enhanced_notifs")

    channel.addEventListener("message", (event) => {
        updateTitle(event.data)
    })

    performIntervalUpdate(channel)

    setInterval(() => {
        performIntervalUpdate(channel)
    }, 5000)

    window.addEventListener("beforeunload", () => {
        channel.close()
    })
}

function performIntervalUpdate(channel: BroadcastChannel) {
    const count = getTotalCount()
    updateTitle(count)
    channel.postMessage(count)
}

/**
 * gets the total counts of notifications (chat + other)
 * @returns
 */
function getTotalCount() {
    let chatCount = parseInt(ensuredSelector<HTMLSpanElement>(".fa-inbox + .notification-icon__count").innerText)
    let otherCount = parseInt(ensuredSelector<HTMLSpanElement>(".fa-comment-alt + .notification-icon__count").innerText)
    chatCount ||= 0
    otherCount ||= 0
    return chatCount + otherCount
}

/**
 * updates the title of the window
 * @param count count to display in the title
 */
function updateTitle(count: number) {
    const title = ensuredSelector<HTMLTitleElement>("title")

    // remove if no notifs | update count | add initial count
    if (title.innerText.match(new RegExp("✉ [0-9]* · .*")) != null && count == 0) {
        title.innerText = title.innerText.replace(new RegExp("✉ [0-9]* · "), "")
    } else if (title.innerText.match(new RegExp("✉ [0-9]* · .*")) != null) {
        title.innerText = title.innerText.replace(new RegExp("✉ [0-9]* ·"), `✉ ${count} ·`)
    } else if (count > 0) {
        title.innerText = `✉ ${count} · ${title.innerText}`
    }
}
