import { ensuredSelector } from "../../typescript-lib/src/shared/common"

/**
 * This function gets the name of the user of this script
 * @returns name of the user using this script
 */
export function getUserName() {
    return ensuredSelector(".nav-click-popup.nav-click-popup--user .simple-menu__header.simple-menu__header--link.js-current-user-cover div").innerHTML
}
